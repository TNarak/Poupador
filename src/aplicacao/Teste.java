package aplicacao;

import algoritmo.PesoSort;
import algoritmo.PossivelComando;

import java.util.ArrayList;
import java.util.Random;

public class Teste {
    public static void main(String[] args) {
        ArrayList<PossivelComando> lista = new ArrayList<>();
        PossivelComando p1 = new PossivelComando(1,2);
        PossivelComando p2 = new PossivelComando(2,2);
        PossivelComando p3 = new PossivelComando(3,3);
        PossivelComando p4 = new PossivelComando(4,1);
        lista.add(p1);
        lista.add(p2);
        lista.add(p3);
        lista.add(p4);
        PesoSort pesoSort = new PesoSort(lista);

        for (PossivelComando p :lista){
            System.out.println("peso: " +p.getPeso()+ " comando:"+ p.getComando());
        }
    }
}